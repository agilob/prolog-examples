% factorial of zero is 1 (by definition).

factorial(0,1).


% Recursively calculate factorial of X.
% Reduces X by one at each call and calculates
% factorial for each value until the desired value is reached.

factorial(X,RESULT) :- R1 is X-1,
                       factorial(R1, R2),
                       RESULT is R2 * X. 
