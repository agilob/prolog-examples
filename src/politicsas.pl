askuser :- hypothesize(Politician),
      write('I guess that the animal is: '),
      write(Politician),
      nl,
      undo.

hypothesize(politician1) :- politician1, !.
hypothesize(politician2) :- politician2, !.
hypothesize(politician3) :- politician3, !.
hypothesize(politician4) :- politician4, !.
hypothesize(unknownpolitician).

% definition of first politician
% I wanted to this policitian to have many views
% Also I have him views to present him as a EU-fan, socialist
politician1 :- verify(abortion),
               verify(feminism),
               verify(highTax),
               verify(commonwealth),
               verify(benefits),
               verify(pacifism),
               verify(immigration),
               verify(openedborders),
               verify(diversification),
               verify(europeanunion),
               verify(socialism),
               verify(naturalFood).

% here we have second policitian, they's a nationalist.
politician2 :- verify(prolife),
               verify(lowtax),
               verify(militarism),
               verify(closedborders),
               verify(strongarmy),
               verify(nationalism),
               verify(foodresearch),
               verify(gmo).

% another nationalist, liberalist,
politician3 :- verify(war),
               verify(abortion),
               verify(commonwealth),
               verify(benefits),
               verify(profamily),
               verify(immigration),
               verify(strongarmy),
               verify(nationalism),
               verify(gmo),
               verify(liberalism).

% profamily, liberalist, naturalist
politician4 :- verify(prolife),
               verify(profamily),
               verify(liberalism),
               verify(poverty),
               verify(pacifism),
               verify(nationalism),
               verify(naturalfood).
               


% here we have a definition of a random voter
politician5 :- verify(profamily),
         verify(naturalfood),
         verify(pacifism),
         verify(closedborders),
         verify(liberalism),
         verify(strongarmy),
         verify(lowtax).

similar(abortion, feminism).
similar(hightax, commonwealth).
similar(lowtax, liberalism).
similar(war, militarism).
similar(war, strongarmy).
similar(strongarmy, militarism).
similar(immigration, openedborders).
similar(diversification, immigration).
similar(europeanunion, openedborders).
similar(gmo, foodresearch).
similar(liberalism, lowtax).
similar(liberalism, pacifism).

opposite(gmo, naturalfood).
opposite(prolife, abortion).
opposite(hightax, liberalism).
opposite(poverty, benefits).
opposite(nationalism, socialism).
opposite(commonweatlh, liberalism).
opposite(militarism, pacifism).

% rule checks if one opinion is opposite to another
% checking doesn't care about order of opinions passed
% to this rule.

oppo(P1, P2) :-
    opposite(P1, P2);
    opposite(P2, P1).


% rule checks if one opinion is similar to another
% checking doesn't care about order of opinions passed
% to this rule.

sim(P1, P2) :-
    similar(P1, P2);
    similar(P2, P1).


% this method compares if two opinions are opposite,
% but if they **were not** connected each other in  data-sets above
% examples I made to test:
%
% P1 = strongarmy, P4 = liberalims
% ?- similar(strongarmy, X).
%   X = militarism.
%
% ?- oppo(militarism, pacifism).
%   true .
% 
% ?- similar(pacifism, liberalism).
%   false.
% 
%% but...
% ?- sim(pacifism, liberalism).
%   true.
%
%% so this method travels whole tree in every possible way to
%% find me this:
% ?- still_oposite(strongarmy, liberalism).
%   true .

still_oposite(P1, P4) :- 
        sim(P1, P2),
        oppo(P2, P3),
        sim(P3, P4).

% verify(prolife). included in data-sets
% verify(profamily). - not included in data-sets
%
% ?- does_support(prolife).
%   true .
%
% ?- does_support(profamily).
%   true .

does_support(POLITICIAN, P1) :-
    sim(P1, P2);
    verify(POLITICIAN, P2).
    
    
% rule returns all elements which
% fit to rule suppport(POLITICIAN, Views).
% Returns a list of views
% for a given politician

support_list(POLITICIAN, Views) :- 
    findall(Views,
                (
                verify(POLITICIAN, Views)
                ),
            Views).


% method checks if specified politician declares to be liberal.
% get policitian, get list of views for the politician
% and save the list in Views check if the list contains label liberalims

is_liberal(POLITICIAN) :-
   support_list(POLITICIAN, Views),
   member(liberalims, Views). 


% method can check if specified politician declares to be a socialist OR
% liberalist OR nationalist. Three at a time.
% `member` methods have to be in one bracket!

is_sln(POLITICIAN) :-
    support_list(POLITICIAN, Views), 
    (
    member(socialism, Views);
    member(liberalism, Views);
    member(nationalism, Views)
    ).


/* how to ask questions */

ask(Question) :-
    write('Do you support the following view: '),
    write(Question),
    write('? yes./no.'),
    nl,
    read(Response),
    nl,
    ((Response == yes ; Response == y)
      ->
       assert(yes(Question)) ;
       assert(no(Question)), fail).

:- dynamic yes/1,no/1.

/* How to supports something */
verify(S) :-
   (yes(S) 
    ->
    true ;
    (no(S)
     ->
     fail ;
     ask(S))).
     

/* undo all yes/no assertions */
undo :- retract(yes(_)), 
		fail.
        
undo :- retract(no(_)),
 	    fail.
        
undo.
