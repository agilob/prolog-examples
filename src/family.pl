male(mark).
male(jonah).
female(marie).
female(sophie).
female(marcie).
parent_of(jonah, mark).
parent_of(marcie, mark).
parent_of(sophie, mark).
parent_of(jonah, marie).
parent_of(marcie, marie).
parent_of(sophie, marie).
mother_of(X,Y) :- parent_of(X,Y), female(Y).
father_of(X,Y) :- parent_of(X,Y), male(Y).
